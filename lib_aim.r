library("RSQLite")
library(data.table)

load_course = function ()
{
  "
  ANONID           
  SUBJECT          
  CATALOG_NBR      
  GRD_PTS_PER_UNIT 
  GPAO             
  DIV              
  ANON_INSTR_ID    
  TERM             
  "
  drv = dbDriver("SQLite")
  con <- dbConnect(drv, "aim.db")
  query = "
  select 
    *
  from course
  ;
  "
  res = dbGetQuery(con, query)
  dt = data.table(res) 
  #levels(dt$SUBJECT) = unique(dt$SUBJECT)
  return(dt)
}

load_student = function ()
{
  "
  MAJOR3_DESCR         
  MAJOR2_DESCR         
  MAJOR1_DESCR         
  HSGPA                
  LAST_ACT_ENGL_SCORE  
  LAST_ACT_MATH_SCORE  
  LAST_ACT_READ_SCORE  
  LAST_ACT_SCIRE_SCORE 
  LAST_ACT_COMP_SCORE  
  LAST_SATI_VERB_SCORE 
  LAST_SATI_MATH_SCORE 
  LAST_SATI_TOTAL_SCORE
  SEX                  
  STDNT_GROUP1         
  STDNT_GROUP2         
  MAJOR1_DEPT          
  MAJOR2_DEPT          
  MAJOR3_DEPT          
  ANONID               
  ADMIT_TERM           
  MAJOR1_TERM          
  MAJOR2_TERM          
  MAJOR3_TERM          
  "
  drv = dbDriver("SQLite")
  con <- dbConnect(drv, "aim.db")
  query = "
  select 
    *
  from student
  ;
  "
  res = dbGetQuery(con, query)
  dt = data.table(res) 
  return(dt)
}

distributions_course = function(df){

  # SUBJECT
  png(filename="SUBJECT.png", width=12, height=4, units="in",res=1200)
  par(mfrow=c(1,1), mar=c(10,5,5,1))
  barplot(table(df$SUBJECT), las=3, main="SUBJECT", ylab='number of students')
  dev.off()

  # DIV
  png(filename="DIV.png")
  par(mfrow=c(1,1))
  barplot(table(df$DIV), las=3, main="DIV", ylab='number of students')
  dev.off()

  # CATALOG_NBR
  png(filename="CATALOG_NBR.png", width=24, height=4, units="in",res=1200)
  par(mfrow=c(1,1))
  barplot(table(df$CATALOG_NBR), las=3, main="CATALOG_NBR", ylab='number of students')
  dev.off()

  # GRD_PTS_PER_UNIT
  png(filename="GRD_PTS_PER_UNIT.png", width=12, height=4, units="in",res=1200)
  par(mfrow=c(1,1))
  barplot(table(df$GRD_PTS_PER_UNIT), las=3, main="GRD_PTS_PER_UNIT", ylab='number of students')
  dev.off()

  #GPAO ALL
  png(filename="GPAO_all.png")
  par(mfrow=c(1,1))
  hist(df$GPAO, las=3, main="GPAO all", ylab='number of students')
  dev.off()

  #GPAO Over 4
  png(filename="GPAO_over4.png")
  par(mfrow=c(1,1))
  over_4 = df[GPAO > 4]
  hist(over_4$GPAO, las=3, main="GPAO over 4", ylab='number of students')
  dev.off()

  #GPAO Under 4
  png(filename="GPAO_under4.png")
  par(mfrow=c(1,1))
  under_4 = df[GPAO <= 4]
  hist(under_4$GPAO, las=3, main="GPAO under 4", ylab='number of students')
  dev.off()

  # ANON_INSTR_ID
  png(filename="ANON_INSTR_ID.png", width=12, height=4, units="in",res=1200)
  par(mfrow=c(1,1))
  inst_cnts = table(df$ANON_INSTR_ID)
  sorted_cnts = sort(table(df$ANON_INSTR_ID))
  bigs = as.integer(names(tail(sorted_cnts, 10)))
  plot(inst_cnts, main="ANON_INSTR_ID", ylab='number of students', xaxt='n')
  #axis(side = 1, at=bigs, labels=T, las=3, cex.axis=0.5)
  axis(side = 1, at=bigs, labels=T, cex.axis=1)
  dev.off()

  # TERM
  png(filename="TERM.png", width=12, height=4, units="in",res=1200)
  par(mfrow=c(1,1))
  barplot(table(df$TERM), las=3, main="TERM", ylab='number of students')
  dev.off()

  # group by SUBJECT, DIV
  png(filename="by-SUBJECT-DIV.png", width=12, height=4, units="in",res=1200)
  par(mfrow=c(1,1), mar=c(10,5,5,1))
  aa = df[,.(cnts = length(ANONID)),by=.(SUBJECT,DIV)]
  bb = aa[order(DIV, -cnts)]
  names = paste(bb$SUBJECT, ' (', bb$DIV, ')')
  barplot(bb$cnts, names.arg=names, las=3, main='Group by DIV-SUBJECT', ylab='number of students')
  dev.off()

}

distributions_student = function(df){

  # SEX
  png(filename="SEX.png")
  par(mfrow=c(1,1))
  barplot(table(df$SEX), las=3, main='SEX', ylab='number of students')
  dev.off()

  # HSGPA
  png(filename="HSGPA_all.png")
  par(mfrow=c(1,1))
  hist(df$HSGPA, las=3, main="HSGPA all", ylab='number of students')
  dev.off()

  # HSGPA Over 4
  png(filename="HSGPA_over4.png")
  par(mfrow=c(1,1))
  over_4 = df[HSGPA > 4]
  hist(over_4$HSGPA, las=3, main="HSGPA over 4", ylab='number of students')
  dev.off()

  # HSGPA Under 4
  png(filename="HSGPA_under4.png")
  par(mfrow=c(1,1))
  over_4 = df[HSGPA <= 4]
  hist(over_4$HSGPA, las=3, main="HSGPA under 4", ylab='number of students')
  dev.off()

  # ADMIT_TERM
  png(filename="ADMIT_TERM.png", width=12, height=4, units="in",res=1200)
  par(mfrow=c(1,1))
  barplot(table(df$ADMIT_TERM), las=3, main="ADMIT_TERM", ylab='number of students')
  dev.off()

  # STDNT_GROUP1
  png(filename="STDNT_GROUP1.png")
  par(mfrow=c(1,1))
  barplot(table(df$STDNT_GROUP1), las=3, main="STDNT_GROUP1", ylab='number of students')
  dev.off()

  # STDNT_GROUP2
  png(filename="STDNT_GROUP2.png")
  par(mfrow=c(1,1))
  barplot(table(df$STDNT_GROUP2), las=3, main="STDNT_GROUP2", ylab='number of students')
  dev.off()

  # group by STDNT_GROUP1, STDNT_GROUP2
  png(filename="by-STDNT_GROUP-1&2.png", width=12, height=4, units="in",res=1200)
  par(mfrow=c(1,1), mar=c(10,5,5,1))
  aa = df[,.(cnts = length(ANONID)),by=.(STDNT_GROUP2, STDNT_GROUP1)]
  bb = aa[order(STDNT_GROUP1, -cnts)]
  names = paste(bb$STDNT_GROUP2, ' (', bb$STDNT_GROUP1, ')')
  barplot(bb$cnts, names.arg=names, las=3, main='Group by STDNT_GROUP1-STDNT_GROUP2', ylab='number of students')
  dev.off()

  # group by STDNT_GROUP1, STDNT_GROUP2 with no NA in STDNT_GROUP1
  png(filename="by-STDNT_GROUP-1&2_no_NA.png", width=12, height=4, units="in",res=1200)
  par(mfrow=c(1,1), mar=c(10,5,5,1))
  no_na = df[STDNT_GROUP1 != 'NA']
  aa = no_na[,.(cnts = length(ANONID)),by=.(STDNT_GROUP2, STDNT_GROUP1)]
  bb = aa[order(STDNT_GROUP1, -cnts)]
  names = paste(bb$STDNT_GROUP2, ' (', bb$STDNT_GROUP1, ')')
  barplot(bb$cnts, names.arg=names, las=3, main='Group by STDNT_GROUP1-STDNT_GROUP2', ylab='number of students')
  dev.off()

  # LAST_ACT_MATH_SCORE
  png(filename="LAST_ACT_MATH_SCORE.png", width=12, height=4, units="in",res=1200)
  par(mfrow=c(1,1))
  barplot(table(df$LAST_ACT_MATH_SCORE), las=3, main="LAST_ACT_MATH_SCORE", ylab='number of students')
  dev.off()

  # LAST_ACT_READ_SCORE
  png(filename="LAST_ACT_READ_SCORE.png", width=12, height=4, units="in",res=1200)
  par(mfrow=c(1,1))
  barplot(table(df$LAST_ACT_READ_SCORE), las=3, main="LAST_ACT_READ_SCORE", ylab='number of students')
  dev.off()

  # LAST_ACT_SCIRE_SCORE
  png(filename="LAST_ACT_SCIRE_SCORE.png", width=12, height=4, units="in",res=1200)
  par(mfrow=c(1,1))
  barplot(table(df$LAST_ACT_SCIRE_SCORE), las=3, main="LAST_ACT_SCIRE_SCORE", ylab='number of students')
  dev.off()

  # LAST_ACT_COMP_SCORE
  png(filename="LAST_ACT_COMP_SCORE.png", width=12, height=4, units="in",res=1200)
  par(mfrow=c(1,1))
  barplot(table(df$LAST_ACT_COMP_SCORE), las=3, main="LAST_ACT_COMP_SCORE", ylab='number of students')
  dev.off()

  # LAST_SATI_VERB_SCORE
  png(filename="LAST_SATI_VERB_SCORE.png", width=12, height=4, units="in",res=1200)
  par(mfrow=c(1,1))
  barplot(table(df$LAST_SATI_VERB_SCORE), las=3, main="LAST_SATI_VERB_SCORE", ylab='number of students')
  dev.off()

  # LAST_SATI_MATH_SCORE
  png(filename="LAST_SATI_MATH_SCORE.png", width=12, height=4, units="in",res=1200)
  par(mfrow=c(1,1))
  barplot(table(df$LAST_SATI_MATH_SCORE), las=3, main="LAST_SATI_MATH_SCORE", ylab='number of students')
  dev.off()

  # LAST_SATI_MATH_SCORE
  png(filename="LAST_SATI_MATH_SCORE.png", width=12, height=4, units="in",res=1200)
  par(mfrow=c(1,1))
  barplot(table(df$LAST_SATI_MATH_SCORE), las=3, main="LAST_SATI_MATH_SCORE", ylab='number of students')
  dev.off()

  # LAST_SATI_TOTAL_SCORE
  png(filename="LAST_SATI_TOTAL_SCORE.png", width=12, height=4, units="in",res=1200)
  par(mfrow=c(1,1))
  barplot(table(df$LAST_SATI_TOTAL_SCORE), las=3, main="LAST_SATI_TOTAL_SCORE", ylab='number of students')
  dev.off()


}


