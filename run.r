rm(list=ls())
options(width=Sys.getenv('COLUMNS')) 
graphics.off()

source('lib_aim.r')
df_course = load_course()
df_student = load_student()

distributions_course(df_course)
distributions_student(df_student)

stop('done')



