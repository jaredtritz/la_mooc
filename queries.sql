
/* how many students are not in course table ... about 10% */
select
  count(*)
from
(
select
  distinct ss.ANONID as sid
from student ss
) res1
left join  
(
select
  distinct cc.ANONID as sid
from course cc 
)res2 on
  res1.sid = res2.sid
where
  /*
  not res2.sid is null
  */
  res2.sid is null
;

/* how many students by DIV and SUBJECT */
select
  DIV,
  SUBJECT,
  count(*) as cnt
from course
group by DIV, SUBJECT
order by DIV, cnt desc
;

/* which student groups */
select
  STDNT_GROUP1,
  STDNT_GROUP2,
  count(*) as cnt
from student
group by STDNT_GROUP1, STDNT_GROUP2
order by STDNT_GROUP1, cnt desc
;

/* strange distribution */
select 
  GRD_PTS_PER_UNIT,
  count(*)
from course
group by GRD_PTS_PER_UNIT
order by GRD_PTS_PER_UNIT
;

/* 
There appears to be 4719 instructors for 87 catlog numbers... 
so over 20 years that's about 3 instructors per course
might be interesting to reconstruct time by instructor
*/

/* It looks like about 20 waves of Fall, Winter, Spring, Summer admits here too */
select ADMIT_TERM, count(*) from student group by ADMIT_TERM order by ADMIT_TERM;

/* 
instructors by what they tought and how many times 
it appears some taught an obsurd # of times...
*/
.output tmp1
select
  ANON_INSTR_ID,
  CATALOG_NBR,
  count(*) as cnt
from course
group by ANON_INSTR_ID, CATALOG_NBR 
order by ANON_INSTR_ID, cnt desc
;

.output tmp2
select
  ANON_INSTR_ID,
  CATALOG_NBR,
  count(*) as cnt
from course
group by ANON_INSTR_ID, CATALOG_NBR 
order by cnt desc
;

/* look at just the most frequently appearing instructor */
.output tmp3
select
  ANON_INSTR_ID,
  TERM,
  CATALOG_NBR,
  count(*) as cnt
from course
where ANON_INSTR_ID = 3604
group by TERM, CATALOG_NBR
order by TERM, cnt desc
;



